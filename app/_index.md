---
title: Companion App
---

<div class="p-notification--caution">
    <p class="p-notification__response">
        <span class="p-notification__status">Warning:</span>
        We are still in developing the Companion App for iOS and Android
    </p>
</div>

## We need really help, please follow us [here](developing)

# Android / F-Droid alpha releases

[Link](https://fdroid.gitlab.io/ccc/)
[![https://fdroid.gitlab.io/ccc/](https://fdroid.gitlab.io/ccc/icon.png)](https://fdroid.gitlab.io/ccc/)
