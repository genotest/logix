## Asparagus
Ab Tag 1, 18:00
Asparagus ist die firmware, die auf den card10s geladen ist, die an Tag 1 ab 18 Uhr ausgegeben wurden.

# Das Firmware-Release auf deinem card10 updaten
* lade die aktuelle Firmware `.zip` Datei auf deinen Computer herunter
* extrahiere sie
* setze dein card10 in den [USB storage modus](/de/gettingstarted#usb-storage)
* Kopiere die Dateien, die du gerade extrahiert hast, in den Hauptordner
* wirf dein Gerät aus (wenn du das auf der Kommandozeile unter Linux tust, vergiss das `sync` nicht)
* Schalte dein card10 aus und wieder an.

