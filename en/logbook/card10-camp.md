---
title: D0 - Traveller Aki
---

## logbook entry

Log Entry D0, Traveller Aki
I somehow came here on purpose, seeking the unknown, the chaos and surprises. From the moment on I arrived, everything happening was a surprise yet strangely familiar - not only to me, but to all the travelers gathering here.

This logbook is my attempt to preserve the knowledge I gained and unhinge it from time itself.

I do not know most travelers here, yet, as always when I leave the old world and enter places like this one, the interactions are heartfelt, respectful and filled with genuine interest, particularly towards our distinctive differences.

As my free thoughts, wild ideas and my ways move and name myself have not attracted any negative attention, I can spend most of my time here I interacting with other travelers and the environment.

And these interactions are enriching in many, many different ways, as the travellers wear a CARD10 around their wrists.

CARD10 connects to each other - they call it Cardiacard10, as far as I know, or sync. One thing all CAD10s show is a special time we all share here. It started with the opening talk some travelers prepared and it counts all the ongoing hours since then, I do not know if it will stop when the last workshop here is finished or if this new time that started will go on?


## conclusion:

There will have been been a device at camp this year that we will have been wearing on our wrists. We need to uncover and (re)produce this device till camp?

#### demands
* find out how this device is built
* develope a time format "camp time" with ongoing hours during the camp
